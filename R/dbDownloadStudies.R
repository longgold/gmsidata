#' Download study data
#' @param db Database connection
#' @param study_id Vector of study IDs to download
#' @param all Boolean; whether to download all studies in the database
#' @return Returns a list where each element corresponds to one participant's data
#' @export 
dbDownloadStudies <- function(db, study_id = NULL, all = FALSE) {
  assertthat::assert_that(
    (!is.null(study_id) && is.numeric(study_id)) ||
      ((!is.null(all)) && all)
  )
  if (!all && length(study_id) == 0) NULL else {
    df <- suppressWarnings(
      "SELECT session.session_id, session.study_id, study.label AS study_label, 
      session.participant_id, session.pilot, session.finished,
      session.time_created, session.time_last_modified,
      data
      FROM session LEFT JOIN study
      ON session.study_id = study.study_id
      %s 
      ORDER BY study_id;" %>%
        sprintf(
          if (all) "" else {
            "WHERE session.study_id IN (%s)" %>%
              sprintf(paste(study_id, collapse = ","))
          }
        ) %>% print %>%
       (function(x) DBI::dbGetQuery(db, x))
    )
    out <- list()
    for (i in seq_len(nrow(df))) {
      out[[i]] <- as.list(df[i, ])
      out[[i]]$time_created <- as.POSIXct(out[[i]]$time_created,
                                          format = "%Y-%m-%d %H:%M:%S")
      out[[i]]$time_last_modified <- as.POSIXct(out[[i]]$time_last_modified,
                                                format = "%Y-%m-%d %H:%M:%S")
      out[[i]]$finished <- as.logical(out[[i]]$finished)
      out[[i]]$pilot <- as.logical(out[[i]]$pilot)
      out[[i]]$data <- GMSIConcerto::char_to_obj(out[[i]]$data)
    }
    out
  }
}