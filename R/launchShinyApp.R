#' Launch Shiny app
#' Launches the shiny app that can be used to manipulate the Gold-MSI database.
#' @param ... Extra parameters to be passed to shiny::runApp()
#' @return None
#' @export
launchShinyApp <- function(...) {
  shiny::runApp(appDir = system.file("application", package = "GMSIData"),
                ...)
}