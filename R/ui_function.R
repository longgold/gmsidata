library(shiny)
library(shinyBS)
library(shinyjs)
library(shinyWidgets)

ui <- function() {
  shiny::fluidPage(
    shinyjs::useShinyjs(),
    shiny::uiOutput("ui")
  )
}

makeUI <- function(logged_in = FALSE) {
  assertthat::assert_that(
    is.logical(logged_in),
    assertthat::is.scalar(logged_in)
  )
  if (logged_in) makeMainUI() else makePasswordUI()
}

makePasswordUI <- function() {
  shiny::div(id = "login",
             shiny::wellPanel(
               shiny::h2("Login"),
               shiny::br(),
               shiny::textInput("login.username", "Username"),
               shiny::passwordInput("login.password", "Password"),
               shiny::br(),
               shiny::actionButton("login_submit", "Log in"),
               style = "max-width:200px; position: absolute; top: 40%; left: 50%; margin-top: -100px; margin-left: -150px;",
               align = "center")
  )
}

makeMainUI <- function()
{
  shiny::shinyUI(shiny::fluidPage(
    
    # Application title
    shiny::titlePanel("Data storage"),
    
    shiny::sidebarLayout(
      shiny::sidebarPanel(
        shiny::tags$p(shiny::tags$strong("Refreshing")),
        shiny::tags$p(shinyBS::tipify(shiny::actionButton("btn_refresh_table", "Refresh table"),
                                      "Refreshes the table by submitting a query to the database.")),
        shiny::tags$p(shiny::tags$strong("New study")),
        shiny::tags$p(
          shinyBS::tipify(shiny::actionButton("new_study_trigger", "New study"),
                          "Creates a new study.")
        ),
        shiny::tags$p(shiny::tags$strong("Select studies")),
        shiny::tags$p(
          shinyBS::tipify(shiny::actionButton("btn_select_all_studies", "Select all"),
                          "Selects all the studies in the table."),
          shinyBS::tipify(shiny::actionButton("btn_deselect_all_studies", "Deselect all"),
                          "Deselects all the studies in the table.")
        ),
        shiny::tags$p(shiny::tags$strong("Labels")),
        shiny::tags$p(
          shinyBS::tipify(shiny::actionButton("edit_label_trigger", "Edit label"),
                          "Edits the label for the selected study.")
        ),
        shiny::tags$p(shiny::tags$strong("Move selected studies to folder")),
        shiny::tags$p(shinyBS::tipify(shiny::actionButton("btn_move_to_current", "Current"),
                                      "Moves the selected study/studies to the Current folder."),
                      shinyBS::tipify(shiny::actionButton("btn_move_to_archive", "Archive"),
                                      "Moves the selected study/studies to the Archive folder."),
                      shinyBS::tipify(shiny::actionButton("btn_move_to_trash", "Trash"),
                                      "Moves the selected study/studies to the Trash folder.")),
        shiny::tags$p(shiny::tags$strong("Lock studies")),
        shiny::tags$p(shinyBS::tipify(shiny::actionButton("btn_lock_studies", "Lock"),
                                      "Locks the selected study/studies, disabling new testing sessions."),
                      shinyBS::tipify(shiny::actionButton("btn_unlock_studies", "Unlock"),
                                      "Unlocks the selected study/studies, enabling new testing sessions.")),
        shiny::tags$p(shiny::tags$strong("Download studies")),
        shiny::tags$p(shinyBS::tipify(shiny::downloadButton("btn_download_selected_studies", "Download selected"),
                                      "Downloads the selected study/studies as an .RDS file that can be read into R using the readRDS function."),
                      shinyBS::tipify(shiny::downloadButton("btn_download_all_studies", "Download all"),
                                      "Downloads all studies in the database as an .RDS file that can be read into R using the readRDS function.")),
        shiny::tags$p(shiny::tags$strong("Log out")),
        shiny::tags$p(
          shinyBS::tipify(shiny::actionButton("btn_log_out", "Log out"),
                          "Logs out of the interface."))
      ),
      shiny::mainPanel(
        shiny::tags$p(
          shinyWidgets::radioGroupButtons(inputId = "folder",
                                          choices = c("Current", "Archive", "Trash"))
        ),
        DT::dataTableOutput("dt_studies"),
        shiny::textOutput("dt_studies_last_updated")
      )
    ),
    # Pop-up windows
    ## Edit label
    shinyBS::bsModal("edit_label_popup", "Edit label",
                     "null", size = "small",
                     shiny::wellPanel(shiny::textInput("new_label",
                                                       "New label",
                                                       placeholder = "e.g. Queen Anne 2016"),
                                      shiny::actionButton("finalize_edit_label",
                                                          "Save"))),
    ## New study
    shinyBS::bsModal("new_study_popup", "New study",
                     "new_study_trigger", size = "small",
                     shiny::wellPanel(shiny::textInput("new_study_label",
                                                       "Label",
                                                       placeholder = "e.g. Queen Anne 2016"),
                                      shiny::actionButton("finalize_new_study",
                                                          "Save")))
  ))
}
