# README #

This Shiny app provides the front-end interface for the Gold-MSI MySQL database.

## Installing the package

In R:

`devtools::install_bitbucket("longgold/gmsidata")`

In terminal: 

`sudo touch /usr/local/lib/R/site-library/GMSIData/application/restart.txt`

## Installing the Shiny app

Once you've installed the package, you need to point Shiny Server to the package location.

Open the config file:

`sudo nano /etc/shiny-server/shiny-server.conf`

Insert the following within the server function:

````
  location /gmsidata {
    app_dir /usr/local/lib/R/site-library/GMSIData/application;
    log_dir /var/log/shiny-server;
  }
````

## Config files

A config file should be created at the location `/config/GMSIData.R`, containing database connection information in the following format:

````
db <- list(
  host = "???",
  user = "???",
  password = "???"
)
````

Note that we don't store these details directly in the source code for security reasons.

## Example usage

```
# Connect to the database
db <- GMSIData::db_connect()

# Register a new participant and get their unique session id
participant_id <- "my participant" # arbitrary string ID, duplicates allowed
study_id <- 13 # unique ID indexing study to save data into 
pilot <- FALSE # whether or not the session is a pilot session
session_id <- GMSIData::dbNewParticipant(db = db,
                                         study_id = study_id,
                                         participant_id = participant_id,
                                         pilot = pilot)

# Save 
data <- mtcars # arbitrary object to save, no structural constraints
finished <- TRUE # whether the participant has now finished (no more data to write)
GMSIData::dbUpdateData(db = db,
                       study_id = study_id,
                       session_id = session_id,
                       data = data,
                       finished = TRUE)
```

## Example appending usage

Participant data can also be built incrementally.
The participant data object initialises as a list.
The `dbAddData` function appends new data to this list.
Each call of `dbAddData` extends the previous data object by adding
an element to the end of the list.

The current implementation is relatively inefficient as (under-the-hood)
it pulls all pre-existing data for that participant each time 
a call is made to `dbAddData`. This is necessitated by the current
database architecture.

See below for example usage:

```
# Connect to the database
db <- GMSIData::db_connect()

# Register a new participant and get their unique session id
participant_id <- "my participant" # arbitrary string ID, duplicates allowed
study_id <- 13 # unique ID indexing study to save data into 
pilot <- FALSE # whether or not the session is a pilot session
session_id <- GMSIData::dbNewParticipant(db = db,
                                         study_id = study_id,
                                         participant_id = participant_id,
                                         pilot = pilot)
                                         
# Save new data objects
dbAddData(db, study_id, session_id, 
          data = Sys.time(),
          label = "Time started",
          finished = FALSE)
dbAddData(db, study_id, session_id, 
          data = runif(5),
          label = "Results from test module 1",
          finished = FALSE)
dbAddData(db, study_id, session_id, 
          data = rnorm(5),
          label = "Results from test module 2",
          finished = FALSE)
dbAddData(db, study_id, session_id, 
          data = Sys.time(),
          label = "Time finished",
          finished = FALSE)

# Inspect the result
getSessionData(db, session_id)
```

## Database fields, by table

### study

The `study` table contains one row for each registered study.

* `study_id` - Integer, uniquely indexes the study (autoincrements)
* `label` - Text label for the study (max. 100 characters)
* `description` - Text description of the study (max. 5000 characters)
* `time_created` - Time that the study was registered
* `time_last_modified` - Time that the study data was last modified
* `locked` - Boolean (1 = true, 0 = false), indicating whether the study is locked, preventing further data entry
* `hidden` - Boolean (1 = true, 0 = false), indicating whether the study is hidden from display on some graphical interfaces

### session

The `session` table contains one row for each registered test session.

* `session_id` - Integer, uniquely indexes the test session (autoincrements; note that this is not the same as Concerto session ID)
* `study_id` - Linked with `session_id` from the `study` table
* `participant_id` - Text ID for the participant, not constrained to be unique, max. 100 characters
* `pilot` - Boolean (1 = true, 0 = false), indicating whether the session was a pilot session, and therefore to be discounted from any data analyses
* `finished` - Boolean (1 = true, 0 = false), indicating whether the participant finished the testing session
* `time_created - Time that the testing session was created in the database
* `time_last_modified` - Time that the testing session was last modified in the database
* `data` - Text representation of the data from the testing session, typically in serialized binary form